// S32 ACTIVITY INSTRUCTIONS

// 1. In the S32 folder, create an activity folder and an index.js file inside of it.
// 2. Create a simple server and the following routes with their corresponding HTTP methods and responses:


let http = require("http");

let port = 4000;

let server = http.createServer((req, res)=> {

	if (req.url == "/" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Welcome to Booking System");
	}

	else if (req.url == "/profile" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Welcome to your profile!");
	}

	else if (req.url == "/courses" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Here's our courses available");
	}

	else if(req.url == "/addcourse" && req.method == "POST"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Add a course to our resources");
	}

	else if(req.url == "/updatecourse" && req.method == "PUT"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Update a course to our resources");
	}

	else if(req.url == "/archivecourses" && req.method == "DELETE"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Archive courses to our resources");

	}


});

server.listen(port);

console.log(`Server is running at localhost: ${port}.`)


// 3. Test all the endpoints in Postman.
// 4. Create a git repository named S32.
// 5. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 6. Add the link in Boodle.